= Conexión con el medidor de potencia

Para configurar la conexión con el medidor de potencia:

+ Acceder a la #link("interfaz-web")[interfaz web] de FreeDS
+ Seleccionar, en el menú desplegable de "selector de modo", en la parte superior derecha, el tipo de medidor de potencia

#figure(
  image("monitorizacion-modos.png", width: 40%),
  caption: [Selector de modos]
)

+ Ir a la página de "configuración"
  - En caso de tener un amperímetro por modbus, completar la información del apartado "Configuración meter"
  - En caso de tener un inversor o amperímetro con conexión WiFi, completar en el apartado #link("origen-datos")["Origen de datos"] los datos necesarios según el tipo de inversor/amperímetro
+ Pulsar el botón "guardar" en la parte inferior de la página de configuración.


Algunos medidores necesitan de una configuración específica.


== Uso con Fronius

Si usas la opción Fronius TCP, sigue los siguientes pasos:

Primero has de activar modbus tcp en el fronius:

+ Introducir la ip de tu fronius en un navegador web
+ Entrar como administrador (debemos tener usuario y contraseña)
+ Click en settings
+ Pestaña `modbus`
+ Marcar `tcp` y verificar que esté en modo `FLOAT`, en modo `INT+SF` no funcionará

La última opción del menú desplegable es “SLAVE”, que se seleccionará si el derivador va a funcionar como secundario o esclavo de un FreeDS principal.

En este menú encontramos arriba a la izquierda la temperatura de los sensores siempre que se hayan instalado (en este caso, uno que registra la temperatura del agua de un termo eléctrico).




== Uso con Solax

Modo de trabajo: “solax Wifi v2 local” con control desde el router, con la lan (Red) de casa.
Se informa de los pasos a seguir, para el modo de trabajo solax wifi v2 local, como si tuviésemos un  esp01 pero sin él. Gracias al firmware modificado, del pincho wifi de \@Christopher101, del grupo Solax FAQ, y la modificación correspondiente  a nivel de software del freeds realizada por \@pablozg:

+ Grabar el firmware modificado de pincho: `618.00122.00_Pocket_WIFI_V2.033.20_20190313_UNLOCKED.usb`. Para ello, nos conectamos  a la wifi del pincho y enlazamos  a 5.8.8.8. Este firmware se puede descargar de https://blog.chrisoft.io/2021/02/14/firmwares-modificados-para-solax-pocket-wifi-v2/

  IMPORTANTE: Hay que actualizar con el fichero de extensión usb. Si se consigue comprimido, con extensión  zip, hay que descomprimir primero. Sinó el pincho wifi quedará inutilizado.
+ Grabar en el esp32 siempre la última beta disponible, actualmente la Beta 12 o posterior.
+ Empezando de nuevo, pulsando el botón prg del esp32 al menos 20 segundos, nos conectamos a la WiFi Freeds y enlazamos con 192.168.4.1. Seleccionamos entonces la Red WiFi del router de casa, con la correspondiente contraseña.
+ Nos indicará la ip asignada por el Router a nuestro esp32, luego nos conectamos a ella para la configuración del freeds. Dentro del menú de FreeDS, en la página de monitorización  seleccionamos arriba a la derecha, el modo de trabajo Solax WiFi v2 local.
+ En la pantalla configuración, en el apartado origen de los datos, ponemos la ip asignada por el Router de casa al pincho wifi, que será  del tipo 192.168.x.x.

  Esto permite olvidarse del esp01 y poder configurar el freeds por la WiFi de casa, mediante la ip que le ha asignado el Router al esp32. Así mismo, si queremos acceder a la configuración del pincho wifi del solax, basta con conectarse a  la ip asignada por el router a éste.

Si no se tiene router, que lo hay sin él, siempre se puede usar el procedimiento anterior con la WiFi del pincho, ya que \@pablozg mantiene también esta opción en la última versión. Para ello, en el punto 2 deberemos seleccionar la red wifi del pincho, sin contraseña y, en el punto 4, pondremos la ip asignada por el pincho, del tipo 5.8.8.x


== SOLAX + Modbus

Teniendo una instalación con un inversor solar SOLAX conectado mediante modbus a un medidor de consumo Eastron SDM230 Modbus.

#figure(
  image("faq-solax.png", width: 30%),
  caption: [Inversor Solax]
)

- Una vez instalado FreeDS en una ESP32 y realizar la configuración para conectar el inversor SOLAX y el ESP32 en la misma red Wifi.
- El SOLAX se conecta a la red wifi mediante un pocket solax wifi.
- importante introducir el firmware `618.00122.00_Pocket_WIFI_V2.033.20_20190313_UNLOCKED.usb_.zip`

- Si una vez realizada la configuración no se consigue leer el dato de la potencia inyectada a la red eléctrica es porque en la configuración del inversor SOLAX está deshabilitado el control de inyección a red.
- Si no se quiere controlar la inyección desde el SOLAX, no deshabilitar la opción export control, sino subir el valor de la potencia a controlar.
- Para poder cambiar los parámetros al SOLAX X1.1:
  - Settings (clave de fabrica 6868)
  - Export control
  - Enable
  - 2000



== Victron Modbus.

Puede suceder que si recibes datos desde un dispositivo Gx de Victron no se muestre la Potencia Solar. Si este es tu caso y tu producción solar procede de inversores solares externos, puedes poner en la consola del FreeDs el comando `useSolarAsMPTT 1` y tu producción solar ya aparecerá registrada.

#figure(
  image("faq-victron-1.png", width: 50%),
  caption: [Datos incorrectos con inversor Victron]
)

#figure(
  image("faq-victron-2.png", width: 50%),
  caption: [Esquema de inversor Victron]
)



== Inversor Ingeteam

Si no muestra bien los datos del inversor, esto es seguramente debido a que seguramente tengas instalado un Meter externo. (el mío es marca Carlo Gavazzi). También es posible que el signo del valor que captures de tu meter este mal o invertido.

Prueba desde la #link("consola")[consola] estos dos comandos:

`useExternalMeter 1`

si el signo está invertido escribe: `invertGridValue 1`





== Inversores Huawei

En las últimas versiones de firmware (acabados en 117, 119 y 121) de inversores Huawei, esta marca ha cerrado los puertos comunicación modbus TCP. Es decir, si tienes una de estas versiones de firmware más recientes, FreeDS no puede acceder a los datos de tu inversor.

Aún así hay cuatro maneras distintas de poder volver a acceder toda la información de tu inversor Huawei:

#set enum(numbering: "1.a)")

+ Hacer un downgrade del firmware de tu inversor Huawei a una versión inferior a la 117.
  + Por ejemplo SPC114.
  + Este método está bien para volver a acceder a los datos de tu inversor pero, desde la aplicación móvil o bien si tienes baterías Huawei recomienda actualizar a su última versión.

+ Comprar un conversor modbus RS485 a WiFi: Elfin-EW11A
  + Este método garantiza poder actualizar el firmware de tu inversor Huawei a la última versión. Y poder acceder a todos los datos de tu inversor. La única diferencia es que deberás acceder a la IP de este pequeño adaptador WiFi en lugar de la IP del inversor.
  + Este método es interesante ya que permite conectar varios clientes a consultar los datos de tu inversor en paralelo.
  + Más información del proceso:

    #link("https://www.youtube.com/watch?v=kNUtZrrfsXs")[Inversor Huawei y Modbus RTU: via WIFI y SIN CABLES]

    Se necesita acceder al inversor por cable para acceder a la interfaz modbus.

    Aquí tienes un tutorial completo para vincular Elfin-EW11A:
    https://drive.google.com/file/d/1JFV_x4oxWyBEfRA3PxbYJtYIPZt-eY3Y/view?usp=sharing

+ Comprar el adaptador original de Huawei para conectarte a través de cable RJ45 al inversor.
  + Método válido pero algo más caro.

+ Instalar un shelly EM, y establecer la comunicación de datos desde el shelly em (No disponible si se tiene baterias)

  https://www.shellyespana.com/contador-de-autoconsumo-fotovoltaico-shelly-em-2-pinzas-de-120a/

#figure(
  image("faq-shelly.png"),
  caption: [Esquema de instalación huawei+shelly]
)

Usa este truco para forzar la comunicación de tu Huawei con FREEDS

Con un SUN2000, hay que entrar en la APP Fusión Solar, "Puesta en servicio del dispositivo". conectarse a la WIFI propia del dispositivo, "SUN2000-......".

+ Acceder con clave de instalador.  En "Ajustes'' > Configuración de la comunicación> Ajustes de parámetros del Dongle> Modbus TCP> Conexión" Hay que parametrizar Habilitar(sin restricciones).
+ Conectar la ESP32 a la WIFI del SUN 2000, seleccionar el inversor Huawei de la lista,  e indicar esta parametrización en el menú de configuración.
+ El puerto no es ya el 502 sino el 6607 y el meter id tiene que ser "0"

/*
#figure(
  image("faq-modbus-6667.png", width: 40%),
  caption: [Configuración de mosbud en puerto 6667]
)
*/


== Monitor Envoy de Enphase

En este punto se describe la forma de configuración del Monitor Envoy-S de Enphase, que monitoriza y gestiona la producción de nuestros paneles solares mediante microinversores IQ7+ o similares de la marca.

#figure(
  image("faq-enphase.png", width: 50%),
  caption: [Inversor Enphase]
)

Nos conectamos al FreeDS y desde la primera pantalla, en su parte superior derecha seleccionamos Enphase Envoy como origen de datos.


Ahora nos vamos directos a “Configuración” del menú izquierdo. Una vez ahí, en el apartado “ORIGEN DE LOS DATOS” introducimos la dirección IP de nuestro monitor Envoy y la clave:


Para obtener la IP de nuestro Envoy introducimos en un navegador conectados desde la misma red “http://envoy.local”, se nos abrirá una web con información. Entre otros datos la IP del WIFI y del ETHERNET, escogeremos según tengamos conectado el Envoy a nuestra red. Preferiblemente ethernet si lo tenemos conectado para evitar problemas de conectividad. En nuestro caso lo tenemos mediante un WIFI (versión 5) y no ha dado problemas.



Para obtener la clave de instalador de nuestro Envoy primero necesitaremos el número de serie del equipo que conseguiremos en la web con información que ya hemos mencionado y lo introduciremos en el siguiente generador online: https://blahnana.com/passwordcalc.html



En la misma sección de configuración del FreeDS ajustamos el tiempo máximo de error a 10000ms y el tiempo de adquisición de datos a 200ms.




== Solar Edge

+ Hay que activar en el inversor el protocolo MODBUS sobre TCP, que viene desactivado por defecto, este cambio de configuración solo lo puede hacer un instalador o remotamente el servicio técnico de SOLAREDGE (el usuario no tiene privilegios para modificar la configuración del inversor), yo llamé al servicio técnico y me lo activaron de forma remota.

+ Normalmente el inversor estará configurado para obtener una IP en nuestra red de forma dinámica ,mediante el servidor DHCP del router, y esta concesión de IP tiene un tiempo de caducidad (Leased Time) tras el cual tendrá que renovar la solicitud y se le concederá otra diferente. Como en la configuración del FreeDS debemos tener una IP fija (del inversor) a la que acudir, (si no habría que estar localizando la nueva IP y re-configurar FreeDS). Tenemos dos opciones para solucionarlo:

  + Que un instalador configure el inversor con una IP fija dentro de nuestra red (el servicio técnico   me comunicó que no tenían capacidad de realizarlo de forma remota).

  + La opción más sencilla, crear en nuestro router una regla para que le asigne siempre la misma IP en el rango de direcciones del servidor DHCP. Para esto tenemos que entrar en el router y averiguar la dirección MAC de nuestro inversor (no es la que aparece en la pegatina lateral, esa es la del adaptador WIFI del propio inversor, y en mi caso al menos, la conexión de red se realiza con cable ETHERNET y conexión a un extensor WIFI,  solo cambiaba un dígito).

Una vez que sabemos la MAC creamos la regla, que en el caso del router Movistar aparece como "Static IP Lease List" dentro de "Configuración avanzada->Advanced Setup->LAN", ponemos la MAC y le asignamos la IP que queramos dentro del rango DHCP siempre que no esté ocupada, lo más fácil es poner la misma que tenga ya asignada.



== Vincula FreeDS con Shelly EM.

https://www.youtube.com/clip/Ugkxmg_-_g34WVPRR5ZIT6FoxOXqGKa640u5



== Vincula FreeDS con Shelly 3EM (instalaciones trifásicas)
Un Shelly 3EM tiene hasta 3 pinzas para medir tres puntos distintos en la instalación eléctrica.

#figure(
  image("shelly-3em.png", width: 40%),
  caption: [Amperímetro shelly trifásico]
)

FreeDS soporta 2 modos de funcionamiento. FreeDS viene por defecto configurado para una instalación monofásica.

+ Monofásico: En una instalación monofásica, la primera primera pinza se utiliza para medir el inversor fotovoltaico. La segunda pinza se utiliza para medir la entrada/salida de electricidad en el contador general. La tercera pinza se utiliza para medir cualquier otro punto independiente del funcionamiento de FreeDs.

+ Trifásico: En una instalación trifásica, cada pinza del del Shelly 3EM se coloca en una fase del cuadro eléctrico.

  Una vez colocadas las pinzas, se debe introducir en la consola de FreeDS un comando para que sume las 3 fases y se interpreten como si fuera solo una. El comando a introducir en la consola de FreeDS es `use3PhasePower 1`

#figure(
  image("shelly-3em-app.png", width: 40%),
  caption: [Aplicación de shelly con trifásico]
)

