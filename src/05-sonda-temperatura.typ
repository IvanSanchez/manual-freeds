= Sonda(s) de temperatura

#figure(
  image("temperatura-sonda.png", width: 50%),
  caption: [Sonda de temperatura]
)

#figure(
  image("temperatura-diagrama.png"),
  caption: [Diagrama de conexión para sonda(s) de temperatura]
)

Sonda temperatura digital con protocolo comunicación bus 1-wire. #link("https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf")[Datasheet sonda DS18B20]
