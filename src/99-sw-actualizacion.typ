= Actualizar FreeDS en el ESP32

Esta opción nos permite actualizar el firmware directamente con nuestro navegador web, no siendo necesario conectar al ordenador por usb el ESP32 para cambiar de versión. Podremos seleccionar el archivo directamente desde nuestro PC y actualizar (las actualizaciones son rápidas y sencillas, al final de cada actualización el ESP32 hará un reinicio automático para cargar desde cero el nuevo firmware).

== Para versiones Beta

IMPORTANTE: para actualizar la versión Beta 13 es necesario tener ya instalada la Beta 11 o Beta 12. No actualizar a la beta 13 desde la web si se tiene cualquier versión inferior.

Para actualizar tanto el firmware del ESP32 es necesario subir primero el fichero `firmware.bin`.

#figure(
  image("fw-update-1.png"),
  caption: [Subiendo fichero `firmware.bin`]
)

Después subir el fichero `littlefs.bin`.

#figure(
  image("fw-update-2.png"),
  caption: [Subiendo fichero `littlefs.bin`]
)

Una vez actualizado recordar pulsar CTRL+F5 en el navegador. Esto vacía la caché del navegador y evita que se vean datos incorrectos en FreeDS.

== Para versiones anteriores

Para actualizar tanto el firmware del ESP32 como la web el mejor método a seguir es el mismo que para cargar el software completo detallado en #link("carga-firmware")[@carga-firmware].

Aunque si se quiere intentar actualizar a través de web los ficheros a subir primero el fichero `firmware.bin` y luego el `spiffs.bin`.

== Recomendaciones

- Es recomendable realizar una restauración a “Fábrica” pulsando 10 segundos el botón PRG en el propio ESP32. Esto obligará a volver a introducir los parámetros de nuestra instalación.

- Borrar la caché del navegador. En algunos navegadores (ej. Chrome, Firefox) se deberá realizar este paso para que los datos de monitorización aparezcan en pantalla correctamente.

