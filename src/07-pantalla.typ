= Pantallas de placa de control

Durante el funcionamiento normal la placa de control ESP32 muestra información sobre el funcionamiento en su pantalla.

Pulsa brevemente el botón `PRG` para cambiar entre las pantallas disponibles.

#figure(
  image("pantalla-esp32-01.png"),
  caption: [Pantalla principal]
)

#figure(
  image("pantalla-esp32-02.png"),
  caption: [Otras pantallas disponibles]
)

