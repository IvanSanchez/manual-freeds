= Meters modbus

Si tu instalación de FreeDS tiene un inversor o un amperímetro que puede proporcionar datos por WiFi, ignora esta sección.

== EASTRON SDM120CT-MV

#figure(
  image("modbus-conexiones.png"),
  caption: [Conexiones para medidor modbus Eastron SDM120CT-MV]
)

Conectar Toroidal en la fase general, fijándose en la flecha que tiene de flujo la pinza,  y dirigiendo esta  hacia el consumo. Por defecto viene configurado el meter con 2400 baudrate y dirección 1 de Modbus. Habrá que ponerlo en el apartado de configuración de FreeDS y poner 1000ms de toma de datos.En mi caso para que el Modbus se activara, leí en el manual que hay que pulsar el único botón del meter hasta que ponga (-SET-). En ese momento se inicia el modbus y   empieza a leer datos el FreeDS.Reiniciar todos los equipos incluido el Meter cuando estén configurados, para empezar a funcionar.

A otros usuarios les funcionó: Primero conectarla placa directamente al esp32 sin nada más. Alimentala a 3.3V y conecta solo el cable de tx hasta que se encienda el led, ajusta la velocidad de lectura de datos a 250ms, para que el ciclo sea más continuo, una vez veas parpadeando el tx, ya puedes conectar el otro cable y conectar al meter. Si no recibes respuesta del meter en unos 10 segundos, invierte los cables que van al meter. Si así tampoco funciona revisa la continuidad de los cables que estás usando para evitar que el fallo sea debido a un cable cortado.
