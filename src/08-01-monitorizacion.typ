
== Monitorización

Los valores y conceptos de esta sección variarán en función del medidor de datos seleccionado. En la pantalla aparecen dos pestañas (Resumen y Detalles).

En el desplegable de la esquina superior derecha se selecciona el tipo de medidor de datos que va a recibir nuestro FreeDS. Para los Fronius, el recomendado es Fronius API (más sencillo de programar).


== Interfaz de usuario de monitorización

Más abajo (dependiendo del medidor de datos seleccionado) nos muestra información de:

#figure(
  image("monitorizacion.png", width: 50%),
  caption: [Información de monitorización]
)

/ Potencia solar: es la que está generando nuestras placas en ese instante.

/ Potencia de red: muestra la cantidad de energía de la red eléctrica. Si no se ha cambiado la configuración correspondiente de cambio de signo, aparecerá en positivo si se está exportando a la red y con signo negativo si se está importando de la misma.

/ Voltaje/intensidad:  (si las lecturas las proporciona un meter)

/ Factor Pot/Frec: Son los valores de Factor de Potencia (Calculado por el meter; es la relación entre potencia activa y potencia aparente. Cuanta más carga resistiva esté conectada será más cercano a 1. Conforme se conectan cargas capacitivas el factor de potencia se reduce. 1 es un valor ideal, lo habitual es un valor de 0.95 o más.

/ Energía solar diaria: nos da un estimado de la producción acumulada ese día

/ Potencia string 1 y 2: es la potencia y voltaje actual generada por cada uno de los strings solares (siempre y cuando se hayan instalado los paneles en dos circuitos independientes, o sea que nos lleguen dos cables rojos y negros al inversor). Habitualmente nos los instalan todos juntos en una única línea (un sólo cable rojo y uno negro).

/ PWM: nos muestra el % de PWM, si está en modo manual o auto, y la ESTIMACIÓN de la derivación en vatios (w), calculada como el % del valor que se haya informado en el campo “potencia de carga conectada” de la sección “Salidas”. Dicha estimación corresponde únicamente a los W teóricos que la carga puede consumir, freeDS no mide en ningún momento el consumo real conectado a la salida del gestor.

Ej: si se ha informado una carga de 1500W en “Salidas” y el PWM está en un 50%, freeDS muestra que el consumo permitido a la salida es de 750W, independientemente de si el aparato conectado está funcionando y consumiendo o no.

/ Salidas: nos muestra el estado de los 4 relés; si están activados estarán en verde. También permite  activarlos y desactivarlos manualmente desde la propia pantalla.

