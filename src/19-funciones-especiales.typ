= Funciones especiales

== Control con Amazon Alexa

(Actualmente esta función no está implementada)

- En configuración del freeds debemos activar "Configuración Alexa"
- Alexa reconocerá los dispositivos, si no lo hace entonces vas a la app Alexa > Dispositivos > + > Añadir dispositivo > Otro > Detectar dispositivos.
Los reconoce como luces. Lo ideal es cambiar los nombres originales por nombres que puedas invocar fácilmente y que alexa los reconozca sin confusión.

- Que puedes controlar por voz tanto en casa como fuera de ella con la app Alexa.
- Encendido y apagado del derivador automático.
- Encendido y apagado del derivador manual (debe estar encendido el derivador automático para que pueda funcionar el manual).
- Control del porcentaje de Derivación en manual
- Control de brillo de la pantalla oled, los valores son entre 1 y 100%

Ejemplos de invocaciones:
+ "Alexa, enciende el gestor"
+ "Alexa, enciende el gestor manual "
+ "Alexa, pon el gestor manual al 80 %"
+ "Alexa, pon la pantalla del gestor al 65%"

No es necesario tener ningún dispositivo Amazon para gestionar los FreeDS desde dentro o fuera de casa pero si la app Alexa de Amazon


== Cambiar entre Master y esclavo
Script para cambiar la función MASTER – ESCLAVO entre varios derivadores FreeDS en función de un selector o una hora determinada del día

Script para cambiar la función MASTER - ESCLAVO entre varios derivadores FreeDS en función de un selector o una hora determinada del día (autoría \@Bisha3)
Tengo dos derivadores, uno con un Acumulador de calor de 1300W en el salón, y otro con un Acumulador de calor de 800W en la habitación. 

Si pongo el derivador del salón como máster y el de la habitación como esclavo, durante el día el acumulador del salón funciona en todo momento y, si sobra energía, la capta el de la habitación. El caso es que a última hora el acumulador de la habitación ha captado muy poca energía respecto al del salón.

Me diréis, pues pon como máster el derivador de la habitación y así te garantizas ese acumulador a tope de carga y lo que sobre se le mete al del salón, pero si pongo el derivador de la habitación como máster y el del salón como esclavo, la habitación se calienta desde primera hora cuando NO LA NECESITAMOS CALIENTE HASTA LA NOCHE. 
https://domotuto.com/script-para-cambiar-la-funcion-master-esclavo-entre-varios-derivadores-freeds-en-funcion-de-un-selector-o-una-hora-determinada-del-dia/

