#import "template-aviso.typ": aviso

= ¿Qué es?

== ¿Para qué sirve?

FreeDS es un _gestor de excedentes_ de autogeneración eléctrica. Es un sistema que trata de consumir la energía eléctrica que se genera y que, de otro modo, se vertería a la red eléctrica. Este consumo se realiza activando _cargas resistivas_ (típicamente un termo de agua caliente sanitaria o una estufa) en los momentos en los que hay excedente.

Por ejemplo: Supongamos que en un hogar con una instalación fotovoltaica, y en un momento dado, se generan 1100W de potencia y los electrodomésticos consumen únicamente 300W. FreeDS encenderá un termo o estufa de tal manera que consuma los 800W restantes.

FreeDS puede realizar este ajuste de varias veces por segundo y de manera constante. Esto consigue minimizar la energía que se importa o exporta a la red eléctrica.

== ¿Por qué usar FreeDS, existiendo las baterías?

Es cierto que un sistema de baterías permite aprovechar mucho mejor los excedentes de producción fotovoltaica. Sin embargo:

- FreeDS es mucho más barato que un sistema de baterías.

- Si ya hay baterías, FreeDS minimiza la carga y descarga de las mismas. Esto ayuda a prolongar su vida útil.

== ¿Qué hace falta?

Una instalación de FreeDS necesita los siguientes componentes:

- *Medidor de potencia:* mide cuánta potencia (en vatios) se está produciendo, y cuánta se está consumiendo.

  A grandes rasgos, los medidores de potencia pueden ser:

  - Integrados en el inversor, con wifi (p. ej. un inversor Victron),
  - Amperímetros con wifi (p. ej. Shelly EM), o
  - Amperímetros conectados mediante #link("https://es.wikipedia.org/wiki/Modbus")[Modbus]

- *Placa de control:* recibe datos del medidor de potencia y calcula cuánta potencia derivar.

  FreeDS fue diseñado para utilizar como placa de control un "ESP32 wifi kit" del fabricante Heltec, una pequeña placa distintiva por ser de color blanco y tener una pantalla OLED de 0.9 pulgadas. Esta placa funciona a 5V y 3.3V.

  #figure(
    image("assets/componentes-esp32-wifi-kit.png", width: 50%),
    caption: [Placa de control]
  )

  La placa de control se programa con *firmware de FreeDS*. Dependiendo de la versión cargada hay más o menos funcionalidades.

- *Placa de potencia:* Controla la conexión eléctrica de 220V hacia la carga resistiva, según las instrucciones de la placa de control.

  #figure(
    image("assets/arquitecturas/pcb-freeds.png", width: 50%),
    caption: [Placa de potencia de FreeDS de 1ª generación. La placa de control encaja sobre las hileras de conectores en la parte izquierda.]
  )

  La placa de potencia utiliza un #link("https://es.wikipedia.org/wiki/Triac")[TRIAC] para controlar la corriente de 220V. El TRIAC se enciende y apaga varias veces por segundo según la #link("https://es.wikipedia.org/wiki/Modulaci%C3%B3n_por_ancho_de_pulsos")[modulación por ancho de pulsos ("PWM")] impuesta por la placa de control.

  Existen varios modelos de placa de potencia diseñados expresamente para FreeDS, aunque hay usuarios que diseñan sus propias placas personalizadas. Dependiendo de la placa se puede conectar más o menos carga resistiva: hay placas que sólo aguantan 500W, y las hay que aguantan 4000W.

- *Carga resistiva*: El electrodoméstico al que se deriva la energía excedente. Es necesario que sea una carga resistiva (es decir, un calentador). Una instalación de FreeDS típica usa un termo de agua caliente o una estufa eléctrica.

  Este electrodoméstico se encenderá y apagará varias veces por segundo, de tal manera que no consume el 100% de potencia eléctrica que podría llegar a consumir.

#aviso[El conectar cargas no resistivas puede dañar los electrodomésticos.

Si un electrodoméstico contiene un motor, *no* lo conectes a la salida de FreeDS.

Si un electrodoméstico contiene electrónica, *no* lo conectes a la salida de FreeDS.]


#figure(
  image("componentes-sistema.png", width: 80%),
  caption: [Componentes de una instalación típica de FreeDS]
)

FreeDS ha sido diseñado con las siguientes caracterśticas en mente:

+ Universalidad: se adapta a distintos sistemas de medida, por lo que puede utilizarse en cualquier sistema de generación eléctrica.
+ Versatilidad: hay diversas configuraciones de hardware, desde placas ya construidas a modelos económicos montados por el usuario.
+ Escalable: pueden usarse varios gestores en una misma instalación al ser posible poner gestores esclavos de un gestor principal.

== ¿Cómo funciona?

A modo de ejemplo, el sistema funciona de la siguiente manera:
+ El medidor de potencia informa a FreeDS que se están produciendo (p. ej.) 1100W, y se están consumiendo (p. ej.) 300W.
+ FreeDS calcula la potencia excedente (en este ejemplo, 800W).
+ FreeDS calcula el porcentaje de PWM para la carga resistiva
	+ Anterioriemte, durante la instalación, hemos informado a FreeDS de la potencia de la carga resistiva (p. ej. nuestro termo consume 2000W).
	+ En este ejemplo: $"PWM" = "800W"/"2000W" = 0.4 =  40%$
+ El TRIAC se enciende y apaga varias veces por segundo, de tal manera que deja pasar corriente de 220V el 40% del tiempo
+ Nuestra carga resistiva está encendida el 40% del tiempo, consumiendo el 40% de su máximo (800W, igual al excedente)

Esta operación se repite constantemente: FreeDS ajusta el porcentaje de PWM para intentar que la carga resistiva consuma el excedente y sólo el excedente.

== ¿Cómo monto mi propio FreeDS?

Los pasos para montar un FreeDS son:

+ Adquirir los componentes
  + Componentes para la placa de potencia
  + Placa de control
  + Caja estanca
+ Montar la placa de potencia
+ (Opcional) conectar accesorios
  - Sondas de temperatura
  - Medidor de potencia modbus
  - Relés
  - Pinza amperimétrica
+ Programar (flashear) la placa de control
+ Configurar el sistema
  + Conectar a red WiFi
  + Configurar conexión al medidor de potencia
  + Definir carga resistiva conectada (y otros parámetros de salida)
  + (Opcional) Configurar accesorios
  + (Opcional) Controlar FreeDS externamente (MQTT / HomeAssistant)

Cada uno de estos pasos corresponde a un capítulo del manual.


