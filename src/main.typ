#set page("a4")
#set text(lang:"es")
#show link: (x)=>underline(text(blue)[#x])
#show ref: (x)=>underline(text(blue)[#x])
// #show heading: (x)=>[#pagebreak(weak:) #x]


#include "00-portada.typ"
#pagebreak(weak: true)

#set page(numbering:"pág 1")

#set heading(numbering: "1.1.1.")
#outline(title: [Índice])
#pagebreak(weak: true)

#include "01-que-es.typ"
#pagebreak(weak: true)
// #include "02-como-funciona.typ"
// #pagebreak(weak: true)
#include "02-lista-componentes.typ"
#pagebreak(weak: true)
#include "03-arquitecturas-potencia.typ"
#pagebreak(weak: true)
#include "04-meters-modbus.typ"
#pagebreak(weak: true)
#include "05-pinza-amperimetrica.typ"
#pagebreak(weak: true)
#include "05-sonda-temperatura.typ"
#pagebreak(weak: true)
#include "06-sw-carga.typ"
// #pagebreak(weak: true)
// #include "09-sw-actualizacion.typ"
#pagebreak(weak: true)
#include "07-pantalla.typ"
#pagebreak(weak: true)
#include "08-interfaz-web.typ"
#pagebreak(weak: true)
#include "09-conexion-medidor.typ"
#pagebreak(weak: true)
// #include "13-configuracion.typ"
// #pagebreak(weak: true)
// #include "11-ajustes-red.typ"
// #pagebreak(weak: true)
#include "12-mqtt.typ"
#pagebreak(weak: true)
// #include "14-salidas.typ"
// #pagebreak(weak: true)
// #include "15-panel-de-control.typ"
// #pagebreak(weak: true)
// #include "16-reiniciar.typ"
// #pagebreak(weak: true)
// #include "17-consola.typ"
// #pagebreak(weak: true)
#include "18-homeassistant.typ"
#pagebreak(weak: true)
#include "19-funciones-especiales.typ"
#pagebreak(weak: true)
#include "20-faq-problemas.typ"
#pagebreak(weak: true)
#include "21-autores.typ"
#pagebreak(weak: true)
#include "22-otros-proyectos.typ"
