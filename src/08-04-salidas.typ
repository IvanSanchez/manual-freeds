#import "template-aviso.typ": aviso

== Salidas

#figure(
  image("salidas.png"),
  caption: [Configuración de salidas]
)

=== Configuración Dimmer PWM

/ Control Auto PWM Activado: activando este cursor Freeds ajusta automáticamente la energía derivada a la carga resistiva en función del consumo de la vivienda y la producción solar. También se puede activar al nivel del panel de control /consola  en el menú lateral del programa web.

/ Potencia de la carga conectada (W): se pone la potencia de la carga resistiva que se va a conectar al derivador (la potencia del termo, por ejemplo). Este valor se usa únicamente para calcular la potencia que la carga puede estar consumiendo en cada momento en función del % de PWM, pero freeDS no mide si dicho consumo se produce.

Si es de más de 2000W se recomienda instalar en la placa de potencia un ventilador que facilita la disipación de calor.

A continuación se configura el parámetro que usa FreeDS para ajustar automáticamente la cantidad de corriente derivada a la carga resistiva. Por defecto los valores negativos indican que se está consumiendo electricidad de la red y los positivos que la instalación fotovoltaica tiene excedentes. Si los valores no aparecen de esta manera girar la pinza del vatímetro (meter) invirtiendo el sentido de medida de la corriente o cambiando la posición de los dos cables de la pinza. La presentación por defecto (consumido en negativo y excedentes en positivo) se puede invertir pulsando el botón correspondiente. La siguiente explicación y ejemplos son con la configuración por defecto, sin cambiar el signo.

/ Potencia objetivo (W): Este es el umbral de vertido fotovoltaico a partir del cual se aumenta la corriente transferida al termo (la producción es mayor que este valor por lo que se puede mandar mas corriente al termo),

Viene preestablecido en 60: cuando se esté vertiendo más de 60W a la red se comenzará a aumentar la potencia enviada a la carga.  Si se vierte menos de 60W se va reduciendo la potencia derivada al termo hasta no derivar nada.

El caso del vertido cero

En el caso de no querer verter nada a red, se deberá establecer un valor negativo (permitiendo consumir algo de la red) para que el derivador pueda actuar y el inversor pueda ir ajustando su producción correctamente. Esto lleva a consumir algo de la red pero nunca se vierte. 

Si se ponen valores negativos, ejemplo -150W, implica que seguirás derivando energía al termo aunque no tengas producción y estés consumiendo hasta 150W de la red. Si consumes más irá reduciendo la energía enviada a la carga hasta cortarla.

Si no te importa verter un poco a la red y prefieres aprovechar bien la producción fotovoltaica en detrimento de consumir nada de la red para el termo debes configurar el inversor para dejar verter una pequeña cantidad (ya no sería vertido 0) pero se aprovecha más la energía producida pues hay retardo entre lo que mide el meter y la toma de decisiones del inversor . Se recomienda establecerlo entre 100 y 200W. Optimizando los valores conseguirás aprovechar hasta la última gota de energía generada vertiendo una mínima cantidad a la red.

El caso de inversión de signo

Si se desea activar la inversión de signo hay que tener en cuenta que los excedentes serán en negativo y el consumo de red en positivo. En esta situación empezamos a tener excedentes con valores negativos. Si se desea transferir excedente a la carga, el valor “objetivo” debe fijarse en 0 o inferior (negativo) .

Configuración de un FreeDS como esclavo

Cuando se configura un FreeDS esclavo los umbrales deben ser más altos que los del maestro  para que se reduzca la derivación del esclavo antes que la del master y los ajustes sean progresivos. 

/ Frecuencia cálculo PWM (ms): La periodicidad de estas comprobaciones se regula en "velocidad cálculo pwm". Para un Fronius configurado con lectura de datos a través de la API con una velocidad de lectura de unos 1500 ms, el valor recomendado es de 2000 - 3000 ms. Si los datos proceden del Modbus TCP  el valor recomendado de velocidad de cálculo sería de 4000 ms con una velocidad de lectura de 250 ms


/ Frecuencia PWM: Las posibilidades son 10 Hz, 12,5 Hz, 500 Hz, 1.0 kHz, 1.5 kHz, 2.0 kHz, 2.5 kHz, 3.0 kHz. 

Cada placa tiene unas preferencias así la placa del letón funciona a 3 kHz,  la placa Ultra Low Cost o Gen3 a 10 Hz. El Low Cost funciona a 10 Hz, pero mejor a 500 Hz. 

Nota: En algún caso, (inversor Growatt SPH 3000) se ha dado interferencia de la placa Ultra Low Cost a 10 Hz con la carga conectada en el  funcionamiento del inversor . Cambiando la frecuencia a 12,5 Hz se soluciona la interferencia.

/ Parche derivador lowcost Desactivado: Hay algunas placas Low Cost con una incidencia en el software que les hace disfuncionar si es tu caso has de activar esta casilla, para todos los demás modelos o las low cost sin este defecto debe estar desactivado. Es fácil saber si está afectada tu placa conectando una bombilla, seleccionando el modo manual, desactivas el parche y pones el porcentaje de PMN al 10%, si al empezar a derivar la bombilla se enciende al máximo de su potencia, tienes que activar el parche, en caso contrario no es necesario activarlo.

/ Límite máximo potencia parche lowcost (1024 - 1232): Para configurar el Low cost con el parche activado se selecciona el modo manual y el porcentaje de PWM al 100% y en ajustar el “límite máximo de frecuencia de PWM” se empieza en 1024 y se va subiendo esta cifra (el límite superior es 1232) hasta que al 100% de PWM la bombilla empiece a parpadear. La selección apropiada del valor de este límite es dos o tres puntos antes de que aparezca el parpadeo asegurándose de que el PWM llega al 100% sin que la bombilla parpadee. 


/ Porcentaje de PWM en modo manual: Si lo que se desea es que la carga funcione al máximo en modo manual se pondrá un 100%. En el caso que se desee que la carga funcione a un nivel inferior se pondrá el porcentaje deseado.

/ Porcentaje de PWM en el master para activar el derivador esclavo: Este campo sólo es modificable en FreeDS esclavos. Si se desea repartir la producción entre varios derivadores esclavos sin llegar a que el derivador principal (maestro o master) esté al 100% para que empiece a derivar. Se puede forzar a que el esclavo se encienda antes que el principal seleccionando 0%.

/ Modo Manual Automático Desactivado: El modo manual automático activado provoca que el derivador pasa automáticamente a modo manual cuando la potencia solar es inferior al valor indicado en “Potencia solar para paso a modo manual”.  Si quieres que el derivador funcione aunque no haya producción solar, por ejemplo para asegurarte de que el termo va a tener agua caliente a pesar de un día nublado.

/ Modo Programador: Puedes programar el FreeDS para que funcione en modo manual (independiente de la producción solar) en un determinado horario seleccionando la Hora Encendido y la Hora Apagado

=== Configuración Salidas para activar periféricos

#figure(
  image("salidas-reles.png"),
  caption: [Configuración de salidas para relés]
)

Las salidas para activar relés funcionan de dos modos pwm o potencia disponible (no funciona con vertido 0).

Cada una de las 4 salidas tienen los siguientes parámetros:

/ Porcentaje %:
/ Potencia Encendido (W):
/ Potencia Apagado (W):

- El "Porcentaje de PWM para activación de salidas por potencia" significa que no se activan las salidas hasta que el pwm esté como mínimo a ese valor y luego cuando se cumplan las condiciones de potencia. Es decir, actúa como condición principal.

- Si el porcentaje de salida está a un valor de 100 o menos, solo funciona en modo pwm, si el valor es 999 funciona usando los valores de salida, con la condición principal.

El valor que usa para activar por potencia es la potencia de vertido, ese es el motivo por el que con vertido cero no funcionará.
Configuración Manual

Es posible activar cada salida individualmente de forma manual.

- Salida 1 
- Salida 2
- Salida 3
- Salida 4
    
ATENCIÓN. No te olvides de guardar usando el botón correspondiente.

#aviso[Por defecto los relés no se activan o desactivan de forma inmediata, tienen un retardo pre-programado. El retardo de apagado son 60 segundos y el del encendido 20 segundos

Los comandos `relayOnTime` y `relayOffTime` introducidos en el menú consola modifican el tiempo para activación y desactivación de los relés, (se debe indicar en segundos).]
