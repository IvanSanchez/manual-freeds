= MQTT <mqtt>

== Configuración

#figure(
  image("mqtt.png", width: 50%),
  caption: [Ajustes de MQTT]
) <ajustes-mqtt> 

/ MQTT Broker:   en este apartado introduciremos la ip del equipo donde tengamos instalado nuestro  del sistema domótico, habitualmente una Raspberry pi o un miniPC
/ MQTT Usuario:  usuario del bloker mqtt.
/ MQTT Password: password de nuestro broker mqtt.
/ MQTT Puerto: puerto de comunicación con el broker mqtt.
/ MQTT Tiempo de publicación: Tiempo en milisegundos en el que publica los topics freeds, se aconseja ser conservador y regular en función de la calidad de la señal wifi y la del broker.
/ Tema R1, 2, 3 y 4: son los topics mqtt de la salida de los relés de los pines 13, 12, 14 y 27.
En el ejemplo de la @ajustes-mqtt el nombre de nuestro freeds es “termo” que lo coje de lo configurado en #link(<ajustes-red>)[Ajustes de Red], dentro de Nombre de Host.

#figure(
  image("pines-reles.png", width: 50%),
  caption: [Pines para los relés]
) <pines-reles>


== Topics MQTT

Actualmente los topics que publica Freeds son los siguientes:

=== Topics MQTT para sensores

/ pw1: Potencia en vatios del string 1
/ pv1v: Voltaje del string 1
/ pv1c: Amperios del String 1
/ pw2: Potencia en watios del string 2
/ pv2v: Voltaje del string 2
/ pv2c: Amperios del String 2
/ wsolar: Producción sumatoria de los strings en vatios
/ invTemp: Tempertura del inversor
/ wtoday:
/ wgrid: Vatios en la red. Con símbolo negativo son consumidos de la red.
/ wtogrid:
/ gridv:
/ calcWatts: Vatios estimados derivando (esto solo es un cálculo, freeds no tiene vatímetro)
/ tempTermo: Temperatura del termo (es necesario instalar una sonda y conectarla al ESP32)
/ tempTriac: Temperatura del triac (es necesario instalar una sonda y conectarla al ESP32)
/ tempCustom: Temperatura con  nombre personalizable (es necesario instalar una sonda y conectarla al ESP32)
/ pwm: % que está derivando a la carga
/ pwmmanvalue: Valor para modificar el pwmm (en modo manual)

ejemplo:
`
  - platform: mqtt
    name: Temperatura interna del acumulador del Salon
    state_topic: acusalon/tempCustom
    unit_of_measurement: °
    
  - platform: mqtt
    name: Watios Derivando a acumulador  Salon
    state_topic: acusalon/calcWatts
    unit_of_measurement: 'W'  

  - platform: mqtt
    name: Temperatura ambiente del Salón
    state_topic: temp_salon/tempCustom
    unit_of_measurement: °
`

=== Topics MQTT para interruptores


/ NUESTROHOST/cmnd/pwmman: Activa/desactiva pwm en modo manual

payload_on: 1

payload_off: 0

ejemplo:
`
  - platform: mqtt
    name: PWM Manual Termo
    command_topic: termo/cmnd/pwmman
    state_topic: termo/cmnd/pwmman
    payload_on: 1
    payload_off: 0
    retain: true
`


/ NUESTROHOST/cmnd/pwm: Activa/desactiva el pwm

payload_on: 1

payload_off: 0

ejemplo:
`
  - platform: mqtt
    name: Activa PWM Termo
    state_topic: "termo/cmnd/pwm"
    command_topic: "termo/cmnd/pwm"
    payload_on: "1"
    payload_off: "0"
    retain: true
`


/ NUESTROHOST/relay/1/CMND: Activa/desactiva relé (igual en 2, 3 y 4)
 
payload_on: "1"

payload_off: "0"

ejemplo:
`
  - platform: mqtt
    name: "Relé Disipador"
    state_topic: "termo/relay/1/STATUS"
    command_topic: "termo/relay/1/CMND"
    payload_on: "1"
    payload_off: "0"
    retain: true
`

=== Nota sobre compatibilidad

FreeDS a partir de la versión 1.1.0005 Beta y superiores hay que enviar los comandos en formato json en lugar de cómo se ha descrito en la sección anterior.

=== PWM ON/OFF

Para encender y apagar el pwm, hay que enviar al nombrehost/cmnd (MQTT) el json: 
- Para encender: 	`{"command":"pwm", "payload":"1"}`
- Para apagar:  	`{"command":"pwm", "payload":"0"}`

Nueva funcionalidad en versión 1.1.0013 Beta:

Si quieres encender y apagar el PWM y estás integrando con NodeRed o Home Assistant, puede resultar interesante para mantener más tiempo la EEPROM del ESP32 y evitar tantas escrituras.

Si es así así pues puedes utilizar en lugar de las opciones anteriores:

- Para encender (modo volátil): 	`{"command":"pwm", "payload":"3"}`
- Para apagar (modo volátil):  	`{"command":"pwm", "payload":"2"}`

Nota: Para el modo volátil, si se reinicia el FreeDS (ESP32) este perderá el estado ya que no se guardará. Deberá ser la integración que mantiene la lógica del estado.

#figure(
  image("mqtt-nodered-1.png", width: 50%),
  caption: [Ejemplo de configuración de comando MQTT para PWM on/off en Node Red]
)

=== PWM AUTOMATICO / MANUAL
Para poner el modo PWM Automatico o Manual, hay que enviar al nombrehost/cmnd (MQTT) el json:
- Para modo Automático:	`{"command":"pwmman","payload":"0"}` 
- Para modo Manual: 	`{"command":"pwmman","payload":"1"}`

#figure(
  image("mqtt-nodered-2.png", width: 50%),
  caption: [Ejemplo de configuración de comando MQTT para PWM auto/manual en Node Red]
)

=== MQTT en Home Assistant

Ejemplo para Home Assistant 

`
switch:
    - name: "PWM ON - OFF Freeds"
      unique_id: PWM_ON-OFF_Freeds
      command_topic: "freeds_nnnn/cmnd"
      state_topic: "freeds_nnnn/stat/pwm"
      payload_on: '{"command":"pwm","payload":"1"}'
      payload_off: '{"command":"pwm","payload":"0"}'
      optimistic: false
      qos: 0
      retain: true

switch:
    - name: "MAN-AUTO Freeds"
      unique_id: MAN-AUTO_Freeds
      command_topic: "freeds_nnnn/cmnd"
      state_topic: "freeds_nnnn/stat/pwmman"
      payload_on: '{"command":"pwmman","payload":"0"}'
      payload_off: '{"command":"pwmman","payload":"1"}'
      optimistic: false
      qos: 0
      retain: true
`

ACTIVACIÓN DE LOS RELES 

/ NOMBREHOST/relay/1/CMND: = Activa/desactiva relé (igual en 2, 3 y 4)
Para los relays hay que hay que enviar al nombrehost/relay/1/CMND el siguiente texto en formato json:
- Para encender: 		`{"command":"relay1","payload":"1"}`    
- Para apagar:		`{"command":"relay1","payload":"0"}` 	

Para el resto de relés sería parecido pero cambiando el número de relé que se quiera actuar (N número de relé opciones 1, 2, 3, 4):
-Para encender: 		`{"command":"relayN","payload":"1"}`
-Para apagar:		`{"command":"relayN","payload":"0"}`


EJECUTAR EL SCRIPT EN HOME ASSISTANT:
Cambio de valor del porcentaje de valor derivado en modo manual con una variable modificable (EN PAYLOAD). Tener en cuenta el nombre de NUESTROHOST del topic
`
service: mqtt.publish
data:
  topic: NUESTROHOST/cmnd/pwmmanvalue
  payload: '{{states.input_number.control_porcentaje_freeds2.state}}'
`

=== MQTT con Domoticz

#figure(
  image("mqtt-domoticz.png", width: 50%),
  caption: [Configuración de MQTT Domoticz]
)

/ DOMOTICZ:   Activa el control con Domoticz
/ Guardar:  Guarda los cambios realizados
/ Reinicializar:  Esto reinicializa el esp a los valores iniciales de Freeds y por tanto borra todas las configuraciones.


=== MQTT Explorer

El programa #link("http://mqtt-explorer.com/")[MQTT Explorer] es de gran ayuda para confirmar las señales MQTT que recibe tu servidor y detectar si hay problemas en algún nombre de los dispositivos y sensores.

#figure(
  image("mqtt-explorer-1.png"),
  caption: [MQTT explorer, conexiones]
)

#figure(
  image("mqtt-explorer-2.png", width: 50%),
  caption: [MQTT explorer, mensajes]
)


=== Compatibilizar por MQTT

Información sobre topics para sistemas no compatibles o que actualmente no se encuentran integrados en FreeDS.

Existen en el mercado muchos tipos de inversores de "marcas" y chinos, Shunts, BMS, etc, no obstante puedes hacer funcionar FreeDS si publicas en tu sistema domótico (OpenHab, Domoticz, HomeAssistant, etc) publicando los topics necesarios para que esa información llegue al dispositivo FreeDS y que funcione en "modo automático".

Te explicamos dos formas de hacerlo, en la primera son publicaciones sobre topics y eligiendo el dispositivo "ICC Solar MQTT" y la segunda por mediante un cadena JSON seleccionando el dispositivo "MQTT JSON TASMOTA".

Para dispositivo "ICC Solar MQTT".- Los topics son:
`
"Inverter/GridWatts"           // Potencia de red
"Inverter/MPPT1_Watts"    // Potencia string 1
"Inverter/MPPT2_Watts"    // Potencia string 2
"Inverter/MPPT1_Volts"     // Tension string 1
"Inverter/MPPT2_Volts"     // Tension string 2
"Inverter/MPPT1_Amps"    // Corriente string 1
"Inverter/MPPT2_Amps"    // Corriente string 2
"Inverter/PvWattsTotal"      // Potencia solar
"Inverter/SolarKwUse"       // Energía diaria solar
"Inverter/BatteryVolts"        // Voltaje batería
"Inverter/BatteryAmps"      // Corriente bateria
"Inverter/BatteryWatts"      // Potencia batería
"Inverter/BatterySOC"        // Nivel batería
"Inverter/LoadWatts"          // Consumo actual
"Inverter/Temperature"       // Temperatura Inversor
`

Para seleccionar "MQTT JSON TASMOTA".- Esta es una cadena a modo de ejemplo:
`{"Status":{"Time":"2021-01-17T11:38:28","ENERGY":{"Pv1Current":21,"Pv2Current":111,"Pv1Voltage":201,"Pv2Voltage":121,"Pv1Power":1.201,"Pv2Power":1.201,"Voltage":207.1,"Today":207.1,"Power":1.9,"Temperature":38,}}}`

Y esta es la información de cada uno de los mensajes:
`
"Pv1Current"   // Corriente string 1
"Pv2Current"   // Corriente string 2
"Pv1Voltage"   // Tension string 1
"Pv2Voltage"   // Tension string 2
"Pv1Power"     // Potencia string 1
"Pv2Power"    // Potencia string 2
"Voltage"        // Tension de red
"Today"          // Potencia solar diaria
"Power"         // Potencia de red (Negativo: de red - Positivo: a red)
"Temperature"  // Temperatura Inversor
`

=== Convertir Voltronic VM III a MQTT para FreeDS (versión beta)

Materiales necesarios:
- Sensor de corriente alterna SCT013 de alta calidad, transformador de corriente de núcleo dividido, 30A, SCT-013-000
  #image("voltronic-sct013.png", width: 30%)
 
- Módulo ADS1015 ADS1115 de 4 canales
  #image("voltronic-ads1115.png", width: 30%)

- DollaTek MAX3232 Puerto Serie RS232 al Conector DB9 del módulo del convertidor TTL
  #image("voltronic-max232.png", width: 30%)

- WEMOS Mega + WiFi R3 ATmega2560 + ESP8266 (32Mb de memoria), USB-TTL CH340G. Compatible Mega, NodeMCU, WeMos ESP8266
  #image("voltronic-esp8266.png", width: 30%)


#figure(
  image("voltronic-conexiones.png"),
  caption: [Conexiones para Voltronic]
)


Configuraciones con programación arduino IDE:

ESP8266 :
https://drive.google.com/file/d/11Nl20QG7gUcIsmmDZXP_Ir2CUaN9cPxO/view?usp=sharing

MEGA:
https://drive.google.com/file/d/1eBQe0PEPr1wBHV_pxNqUNi-am8jDKG3H/view?usp=sharing

Para programar ESP8266 que viene integrado en la placa que paso, hay que poner los dips switch en 5,6,7 ON y el resto OFF.
Para programar MEGA hay que poner los dips switch 1,2,3,4 ON y el resto OFF.

Después de programarlos cada uno con su código, hay que poner los dips switch en 1,2,3,4 ON el resto OFF.

Todos se programan como cualquier tarjeta de arduino.

NOTA: Es una versión BETA, por lo que al rato de funcionar da error, se agradece depurar el código y volver a darnoslo operativo y mejorado. OJO CON LA ALIMENTACIÓN 12Vcc QUE SACAMOS DEL VOLTRONIC III VIENE SIN PROTEGER, PONER FUSIBLE PARA EVITAR DAÑAR EL INVERSOR.

Por último conectar el Rj45 a el voltronic VMIII

#figure(
  image("voltronic-rj.png", width: 50%),
  caption: [Conexión RJ45 de Voltronic VMIII]
)

Así queda mi montaje:
#figure(
  image("voltronic-caja.png", width: 50%),
  caption: [Montaje de Voltronic VMIII finalizado]
)

Para que el FREEDS funcione configurar de la siguiente manera:

#figure(
  image("voltronic-mqtt.png", width: 50%),
  caption: [Configuración MQTT de FreeDS para Voltronic]
)
#figure(
  image("voltronic-configuracion.png", width: 50%),
  caption: [Configuración de FreeDS para Voltronic]
)


