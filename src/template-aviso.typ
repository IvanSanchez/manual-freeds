#let aviso(texto) = rect( 
  width: 100%, 
  stroke: red + 3pt,
  fill: luma(92%) 
)[ #stack(dir: ltr,
spacing: 25pt,
align(horizon, image("assets/Danger.svg", width: 50pt)),
box(width: 350pt,texto)
)] 
