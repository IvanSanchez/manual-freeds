= Carga de firmware FreeDS en ESP32 <carga-firmware>

Para programar (o "flashear") la placa de control ESP32 necesitarás conectarla a un ordenador mediante un cable USB. El #link("https://en.wikipedia.org/wiki/USB_hardware")[conector USB] de la placa de control es un USB 2 micro B. No uses cables USB C, ni USB 3 micro B.

El método depende de tu sistema operativo.

== Windows

a. Descarga el firmware de FreeDS desde https://github.com/pablozg/freeds/releases/ . Descarga el `.zip` de la versión que desees.

b. Descomprime el `.zip`. Algunos usuarios de Windows recomiendan, por comodidad, descomprimirlo en la raíz del disco duro `C:\`

#figure(
  image("fw-windows-1.png"),
  caption: [Ficheros de FreeDS en C:/ en windows]
)

c. Alternativamente, descomprímelo en la carpeta (o "directorio") donde desees tener el material de FreeDS.

d. Abre un terminal de windows (pulsa la combinación de teclas WIN+R y escribe `cmd`).

#figure(
  image("fw-windows-2.png"),
  caption: [Línea de comandos en windows]
)

e. Una vez en el terminal, ve al directorio donde hayas descomprimido FreeDS, mediante el comando `cd`. Si lo has descomprimido en la raíz de `C:\`, ejecuta `cd C:\FreeDS`.

f. Comprueba que estás en el directorio correcto, ejecutando `dir`. Deberías ver un listado de los ficheros descomprimidos.

g. Ejecutar el comando `programarESP32.bat COMX`, donde debes sustituir `X` por el número del puerto serie del ESP32. Es bastante habitual encontrarlo en `COM3`, p. ej. `programarESP32.bat COM3`.

Video tutorial: https://youtu.be/Gw5sJ3fvDY0 

- Puede que necesites instalar drivers para que windows detecte la placa ESP32 como un puerto serie.

- Si tras ejecutar el comando `programarESP32.bat COMX` te aparece el error

  ```A fatal error ocurred: Failed to connect to ESP32: Timed out waiting for packet header```

  , prueba a pulsar el botón `PRG` del ESP32 cuando aparezca la palabra "conectando".

- Para saber qué puerto serie (puerto COM) es el de la placa ESP32, #link("https://superuser.com/questions/1059447/how-to-check-com-ports-in-windows-10")[usa el gestor de dispositivos de Windows].

== Mac/Linux

+ Asegúrate de tener #link("https://github.com/espressif/esptool")[`esptool`] instalado en el sistema.
  - Si usas una distribución de Linux basada en Debian (incluyendo Ubuntu), puedes instalarlo ejecutando `apt install esptool`. Después deberás ejecutar `esptool` en vez de `esptool.py`.

+ Descarga el firmware de FreeDS desde https://github.com/pablozg/freeds/releases/ . Descarga el `.zip` de la versión que desees.

+ Identifica el puerto serie usado, ejecutando `sudo dmesg | grep tty`. Normalmente será `/dev/ttyUSB0` si es el único dispositivo USB conectado.

+ Abre una terminal de comandos

+ Una vez en la terminal, ve al directorio donde hayas descomprimido el firmware de FreeDS, ejecuta p. ej. `cd directorio_donde_hayas_descomprimido_freeds`

+ Borra la memoria del ESP32 ejecutando:

   ```
   sudo esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 --before default_reset --after hard_reset erase_flash
   ```

+ Carga el firmware de FreeDS en el ESP32 ejecutando:
   - Para firmware 1.0.x:
     ```
     sudo esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0x1000 bootloader_dio_80m.bin 0x8000 partitions.bin 0xe000 boot_app0.bin 0x10000 firmware.bin 0x290000 spiffs.bin
     ```

   - Para firmware 1.1-beta:
     ```
     sudo esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0x1000 bootloader.bin 0x8000 partitions.bin 0xe000 boot_app0.bin 0x10000 firmware.bin 0x310000 littlefs.bin
     ```

Se espera que en versiones posteriores a 1.1-beta16 se incluya un #link("https://github.com/pablozg/freeds/issues/85")[script de shell para Linux, similar al `programarESP32.bat` de Windows].

== Configuración inicial

Después de flashear el ESP32, debería resetearse, iniciarse (según @oled-iniciando) y mostrar en su pantalla (según @oled-ap) el mensaje:

  ```
  CONÉCTESE AL SSID:

  FreeDS

  192.168.4.1
  ```

#box[
  #columns(2, [
    #figure(
      image("assets/oled-iniciando.png", width: 50%),
      caption: [Pantalla de la placa de control al encender el sistema]
    ) <oled-iniciando>
  #colbreak()
    #figure(
      image("assets/oled-ap.png", width: 50%),
      caption: [Pantalla de la placa de control preparada para configuración inicial]
    ) <oled-ap>
  ])
]

Tu ordenador debería poder ver una red WiFi con nombre (SSID) "FreeDS". Conecta tu ordenador a esa red.

En un navegador web, visita la IP indicada. Los navegadores modernos suelen intentar conectarse a HTTPS (cifrado) en vez de HTTP (sin cifrar), lo cual puede causar confusión. Para evitar problemas, introduce en tu navegador web la URL `http://192.168.4.1:80/`

Verás una página web con un menú desplegable mostrando todas las redes WiFi que FreeDS es capaz de detectar. Para que FreeDS pueda conectarse a tu red wifi deberás seleccionar el SSID correspondiente, y escribir la contraseña de la red. Pulsa el botón "Guardar".

Una vez realizada la configuración inicial, FreeDS se reseteará e iniciará su modo de funcionamiento normal.

#box[
  #columns(2, [
    #figure(
      image("assets/oled-iniciando.png", width: 50%),
      caption: [Pantalla de la placa de control al encender el sistema]
    )
  #colbreak()
    #figure(
      image("assets/oled-conectando.png", width: 50%),
      caption: [Pantalla de la placa de control al conectarse a red WiFi]
    )
  ])
  #columns(2, [
    #figure(
      image("assets/oled-ip.png", width: 50%),
      caption: [Pantalla de la placa de control al obtener dirección IP]
    ) <oled-ip>
  #colbreak()
    #figure(
      image("assets/oled-info.png", width: 50%),
      caption: [Pantalla de la placa de control en funcionamiento]
    ) <oled-info>
  ])
]

Desconecta tu ordenador de la red WiFi "FreeDS", y conéctalo a la misma red WiFi a la que FreeDS está conectado. Usando un navegador web, visita la IP indicada (según @oled-ip o @oled-info). Si el navegador tiene problemas para conectarse (por intentar usar HTTPS en vez de HTTP), usa una URL con `http://`. Por ejemplo, si la dirección IP de FreeDS es `192.168.1.116`, usa la URL `http://192.168.1.116:80`.

La interfaz web de FreeDS debería pedir usuario y contraseña. Usa el usuario `admin` y contraseña `admin`.

== Reseteo "de fábrica"

Para borrar toda la configuración de FreeDS, incluída la red WiFi a la que debe conectarse, pulsa el botón `PRG` de la placa de control durante al menos diez segundos.

Después de borrar toda la configuración, deberás realizar de nuevo el procedimiento de configuración inicial.


== Actualizar FreeDS

El firmware de FreeDS se puede actualizar de dos maneras:

- Cargando el nuevo firmware desde cero, conectando la placa de control a un ordenador por USB (recomendado).

- Usando el apartado "Actualización" de la interfaz web de FreeDS.

  La actualización mediante la interfaz web se realiza en dos partes:

  + Subir fichero `firmware.bin`
  + Subir fichero `littlefs.bin` (para versiones 1.1-beta) o `spiffs.bin` (para versiones 1.0.x)

