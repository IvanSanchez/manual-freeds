
= Links de componentes

#set table(  columns: (auto, auto),
  inset: 10pt,
  align: horizon
)

== Elementos comunes

Todos los FreeDS necesitan una placa de control, y se recomienda encarecidamente una caja estanca para no dejar elementos eléctricos expuestos.

#table(
[Caja estanca IP66], [https://a.aliexpress.com/_ASHVpT

https://a.aliexpress.com/_AERvOb],
[ESP32 wifi kit], [https://a.aliexpress.com/_AqNIC3

https://a.aliexpress.com/_BPzkGd],

[Cable USB2 micro-B], [https://www.aliexpress.com/item/1005001289715990.html]
)

Algunas placas de control se venden con los pines sin soldar. En caso de duda, consultar con el proveedor.

== Placa de potencia

FreeDS puede usar varias placas de potencia (como se puede ver en #link(<arquitecturas-potencia>)[Arquitecturas de Potencia]). Las opciones (actuales) para obtener una placa de potencia son:

=== Pedir una placa a \@amcalo

Lolo "amcalo" mantiene una lista de espera de personas que quieren una placa de FreeDS, y las monta de manera semiprofesional. Suelen pasar varios meses entre una tanda de pedidos y la siguiente. Lolo se pone en contacto con las personas apuntadas.

La lista de espera está en https://docs.google.com/spreadsheets/d/1-mIZ6MlgMaCQPpuY4FkbIRd1uKQP1vSMtHRRQbU2fl0/edit

Puedes contactar con Lolo mediante Telegram: https://t.me/amcalo

=== Pedir una placa a KRIDA Electronics

Olegs Bugrovs, de Letonia, vende kits completos (placa de potencia + placa de control) ya montados, bajo la marca "KRIDA Electronics".

La "arquitectura Letón" funciona con una placa de control propia (no simplemente un devkit Heltec blanco). Las placas de potencia y de control se adquieren aparte.

Véase #link(<arquitectura-leton>)[Arquitectura Letón] y #link(<arquitectura-leton-prov5>)[Arquitectura Letón prov5].

// === Pedir una placa a SolaresTV
//
// Francisco Ureña

=== Hacer tu propia placa

Los componentes listados aquí son los más habituales para una instalación de FreeDS. Deberás también obtener una placa de circuito impreso (o una placa de prototipado) con un diseño adecuado. Véanse, por ejemplo, #link(<arquitectura-low-cost>)[Arquitectura Low-Cost] y #link(<arquitectura-clinxer>)[Arquitectura Clinxer].

Es posible utilizar componentes distintos a los aquí descritos, pero hazlo sólo si tienes conocimientos de electrónica.

#table(

[Triac], [https://es.aliexpress.com/item/32944601805.html],

[Fuente ACDC], [https://a.aliexpress.com/_AMwBET],
[Disipadores], [https://a.aliexpress.com/_APdxi7],

[Ventilador], [https://a.aliexpress.com/_9fD1Tz

https://a.aliexpress.com/_9IeI54],

[Fusibles], [https://a.aliexpress.com/_9jVP9f],
[Portafusibles], [https://es.aliexpress.com/item/32882043385.html],
[Zócalo para Moc ], [https://a.aliexpress.com/_AcyGLL],
[Porta fusibles], [https://a.aliexpress.com/_Alt7br],
[Conector AC], [https://a.aliexpress.com/_AcyGLL],
[Moc 3041], [https://es.aliexpress.com/item/4000159006955.html],
[Potenciómetro], [https://a.aliexpress.com/_996luh],
[Pines], [https://es.aliexpress.com/item/32986655308.html],
[Botón pulsador], [https://a.aliexpress.com/_9wIMvr],
)

Nota: Para elegir el amperaje del fusible se hará acorde a la fórmula $I=P/V$ eligiendo el amperaje comercial ligeramente superior al que nos dé el cálculo.

Por ejemplo, un termo de 2000 watios: $I = "2000W"/"230V" = 8.69A$ $=>$ elegiremos un fusible de 10A.

\@amcalo mantiene una lista de componentes en https://docs.google.com/spreadsheets/d/1-mIZ6MlgMaCQPpuY4FkbIRd1uKQP1vSMtHRRQbU2fl0/edit#gid=689588561

=== Placas de potencia no epecíficas de FreeDS

Es posible adquirir placas de potencia ya completas. Sin embargo, no disponen de zócalo para la placa de control, que deberá conectarse pin a pin.

#table(
[Placa Low cost ¡Ojo! solo 3A, 690w], [https://a.aliexpress.com/_AeCfE5],
)

== Componentes para Modbus

Estos componentes son necesarios sólo para instalaciones en las que FreeDS se comunica con el amperímetro mediante Modbus.

Si tienes un inversor o amperímetro que se comunica por wifi, no necesitas estos componentes.

#table(
[Conversor RS485], [https://a.aliexpress.com/_9j5SiB

https://www.amazon.es/dp/B07WSD25CC/],

[Meter DDS238], [https://a.aliexpress.com/_AfsKqr],
[Meter SDM120], [https://es.aliexpress.com/item/32503313371.html],
[Meter SDM230], [https://es.aliexpress.com/item/32698830575.html],
)
