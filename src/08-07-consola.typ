== Consola <consola>

FreeDS dispone de una consola o terminal que permite introducir comandos que no están entre las opciones de la interfaz gráfica. Entre otros, estos son los comandos principales. Se introducen de uno en uno y aceptando con INTRO para enviarlas,

/ KwToday 1:

/ KwTotal 1:

/ KwExportToday 1:

/ KwExportTotal 1:

/ flipScreen: gira 180 grados la pantalla oled, se usa junto a la PCB de FreeDS

/ weblog 1: (Para activar, ó 0 para desactivar) Muestra los mensajes de salida de FreeDS en la ventana superior de la consola.

/ tzConfig CET-1CEST,M3.5.0,M10.5.0/3: esta orden sincroniza el tiempo de FreeDS con internet. En este ejemplo corresponde con España peninsular.

/ rebootcause motivo de reinicio: (no siempre es la razón real)

Para configurar en tu huso horario debes consultar la web: https://raw.githubusercontent.com/nayarsystems/posix_tz_db/master/zones.csv 

/ showEnergyMeter 1: (Para mostrar, ó 0 esconder) Estará en la próxima versión, mostrará o ocultará los contadores de vertido / consumo.

Si se conocen los valores recogidos por otro medidor, también se puede introducir tras cada orden, el valor que se desea se tome como punto de partida, por ejemplo:

/ KwToday 1250: ej. Sitúa el contador en 1250

/ useExternalMeter 1: Para utilizar los datos proporcionados por un medidor externo. Con la opción 1 se habilita, con la 0 pasa nuevamente a los datos del inversor.

/ relay4AsFanSwitch 1 / 0: activa / desactiva el control del ventilador, funciona en auto y man según porcentaje indicado en la salida 4

/ tunePID: consulta y ajuste de los parámetros PID del sistema, los cuales sirven para evitar las oscilaciones de potencia (video explicativo del PID: https://youtu.be/WsoaQNw04zs)

Si se envía el comando “tunePID” devuelve los ajustes actuales.
Usando `tunePid X.XX;X.XX;X.XX` (reemplazando las X por los valores deseados) se ajustan dichos valores en el sistema (ej.: `tunePID 0.05;0.06;0.03`)

/ use3PhasePower : Suma las 3 fases de una instalación trifásica en una sola. Comando para usar con un Shelly 3EM. La suma de las 3 fases permite a FreeDS interpretar los excedentes correctamente.


