= Home Assistant

== Mediante integración de FreeDS

Véase https://gitlab.com/IvanSanchez/homeassistant-freeds

== Mediante MQTT


#figure(
  image("ha-mqtt-1.png", width: 75%),
  caption: [Home Assistant con valores de FreeDS mediante MQTT]
)

Se puede conectar FreeDS con Home Assistant mediante MQTT. Para ello es necesario tener:
- Servidor Mqtt
- NodeRed.

Ambos se pueden instalar mediante hassio store “Tienda de complementos” o siguiendo los tutoriales de YouTube.

Para Generar dos Botones que interactúen con Freeds tenemos que ir a Configuración /ayudantes (desde la versión Hassio 2021.12.0 se encuentra en configuración/automatizaciones y escenas/ayudantes)

Tenemos que buscar el botón “AÑADIR AYUDANTE” y pulsar en Despegable. En mi caso voy a crear dos que más adelante conectaremos con NodeRed.

#figure(
  image("ha-mqtt-helper-pwm.png", width: 50%),
  caption: [Ayudante de Home Assistant para PWM on/off]
)
#figure(
  image("ha-mqtt-helper-mode.png", width: 50%),
  caption: [Ayudante de Home Assistant para PWM auto/manual]
)

Ahora solo queda entrar en nodered. Tienes que tener instalado el módulo “node-red-contrib-home-assistant-websocket” y configurado con nuestra ip de Home Assistant.
Tenemos que configurar en FreeDS el servidor MQTT. (Leer #link("mqtt")[@mqtt])

Enlace para descargar el flujo de NodeRed y configuration.yaml: 
https://github.com/wepar2/config-Home-Assistant/tree/main/Freeds
(en el node freeds_6d50 tienes que poner el nombre que tienes asignado, viene en  #link("mqtt")[@mqtt])

#figure(
  image("ha-mqtt-nodered.png"),
  caption: [Configuración de flujo de NodeRed]
)
#figure(
  image("ha-mqtt-nodered-event.png"),
  caption: [Configuración de evento de NodeRed]
)
NodeRed:

En la entidad “state node” tienes que poner el mismo nombre que le asignaste en Home Assistant. 

Se actualizó el flujo en el repositorio de github que está un poco más arriba. Incluye integración con la 1.1.0005 Beta. Esta última hay que pasarle el código mqtt mediante un json.
`{"command":"pwm","payload":"1"}` , El tópico que apunta en las nuevas versiones es: `freedsXXX/cmnd`.

Para versiones antiguas el tópico es `freedsXXX/cmnd/pwm` o `freedsXXX/cmnd/pwmman` (Este solo para versiones 1.1.0 o superiores).

