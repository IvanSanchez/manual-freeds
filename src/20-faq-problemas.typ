= Problemas frecuentes

== FreeDS no muestra la hora y fecha correcta.

#figure(
  image("faq-hora.png", width: 50%),
  caption: [Pie de página web mostrando la hora actual]
)

En el pie de página de control por web de FreeDS se muestran varios datos, entre ellos la hora y fecha actual, si no es así, prueba desde la consola estos dos comandos:

`weblog 1` (intro)
`tzConfig CET-1CEST,M3.5.0,M10.5.0/3` (intro)
Los  (intro) del final no se teclean, solo es para entrar la orden escrita, FreeDS obtiene los datos directamente de internet a través de un servidor NTP.  Si con esta solución no aparece, puede haber un error en la conexión a internet, problemas con el DHCP o al configurar la IP fija. Si no hay conexión a internet, no se verá la hora y no podrá funcionar por ejemplo el modo programador.

Para configurar en tu huso horario debes consultar la web: https://raw.githubusercontent.com/nayarsystems/posix_tz_db/master/zones.csv 

== Mi Esp32 marca poca señal wifi. 
Sí, es cierto. Las soluciones pasan por intentar acercarlo a tu punto de acceso/router, o bien poner un repetidor wifi o bien soldarle 2 alambres de 3 cm segun la foto. ¡ Y ojo que 3 cm son 30 mm, ni 29 ni 31 mm!

Tutorial de ayuda: https://www.instagram.com/tv/CFRgJDJqEMV/


#figure(
  image("faq-antena.png", width: 50%),
  caption: [Antena de 30mm soldada a la placa de control]
)

En caso de querer soldar un cable coaxial para poner una antena externa, cortaremos con un cuchillo, cutter o dremel en los círculos rojos y soldamos el cable según fotos y modelo del esp32. Una vez soldado, comprobaremos con el multímetro que no hay continuidad entre malla y vivo, a la vez, debe haber continuidad entre malla y el pin gnd del esp32. 

#box[
  #columns(2, [
    #figure(
      image("faq-antena-1.png", width: 35%),
      caption: [Puntos de corte para antena externa]
    )
  #colbreak()
    #figure(
      image("faq-antena-2.png", width: 35%),
      caption: [Puntos de corte para antena externa]
    )
  ])
]

#figure(
  image("faq-antena-3.png", width: 40%),
  caption: [Puntos de corte para antena externa]
)


== Actualizar firmware FreeDS o cargar una nueva versión en un ESP32

https://drive.google.com/file/d/1fnrQXJZWctFs027DQTgaY64aE0Xwjd-9/view?usp=sharing 


