= Pinza amperimétrica <pinza-amperimetrica>

Es posible añadir una pinza para que FreeDs sepa si hay consumo a la salida del derivador, la pinza irá conectada al pin 34 según se indica en el esquema siguiente:

#figure(
  image("pinza-amperimetrica.png"),
  caption: [Diagrama de pinza amperimétrica]
)

Eléctricamente, el montaje es igual que el descrito en https://savjee.be/blog/Home-Energy-Monitor-ESP32-CT-Sensor-Emonlib/ : Un condensador de 10µF y dos resistencias para dividir tensión. Las resistencias deben ser iguales, y cada una debe ser de entre 10K$ohm$ y 470K$ohm$.

Para configurar y probar la pinza amperimétrica es posible usar en la #link("consola")[consola] los siguientes comandos:

/ `useClamp 0` / `1`: desactiva / activa el uso de la sonda

/ `clampCalibration`: sin valor indica el valor actual
/ `clampCalibration 36.10`: guarda el valor, p. ej 36.10

/ `clampVoltage`: sin valor indica el valor actual
/ `clampVoltage 220.5`: guarda el nuevo valor, ya que no hay posibilidad de medirlo con la PCB actual. Debes indicar un valor medio según tu zona.

/ `showClampCurrent 0` / `1`: oculta / muestra el valor de intensidad medida por la pinza, ya que al cortar ciclos se falsean las lecturas. Debes poner una carga conocida al 100%  para medir la intensidad consumida en ese momento, e ir ajustando el valor de calibración hasta que coincida.
