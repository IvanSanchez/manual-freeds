== Configuración

=== Cambio de password

/ Cambio de Password: por defecto es “admin” y aquí se puede cambiar a otra de tu elección.
/ Clave de acceso:
/ Nueva clave de acceso:

=== Funcionamiento en Instalación Aislada o con baterías
Se activa desplazando el cursor

/ Porcentaje mínimo de SoC para derivar:
/ Consumo máximo de Batería (W):

Nota de funcionamiento:
Cuando se usan baterías puedes usar dos modos de funcionamiento:

/ Modo Aislada o con Batería: En este se debe configurar el % de SoC a partir del cual se enviarán los excedentes a la carga, y en el campo potencia objetivo se debe configurar el valor de consumo permitido desde la batería.

/ Modo por defecto: En este se debe configurar el consumo máximo de batería que se permite usar y en el campo de potencia objetivo se debe configurar el valor de vertido / consumo de red permitido.

=== Origen de datos <origen-datos>
Se introduce la dirección  “IP” del meter, inversor o herramienta de medida. En caso de estar configurado como esclavo se introduce la IP del FreeDS maestro del que dependerá.
El consumo eléctrico tendrá valores positivos cuando se generan excedentes fotovoltaicos y negativos cuando se consume corriente de la red. En el caso de que el meter le asigne valores al revés se ha de cambiar el signo de los valores activando el cursor correspondiente

/ Cambio de signo en los valores de Red:

Configuración Meter
Baud rate: 300 bps  -  38400 bps                                                             
Meter Id:
Tiempo Máximo De Error
Tiempo máximo de error (ms):
Tiempo De Adquisición De Datos
Tiempo adquisición datos (ms):

=== Configuración Pantalla Oled
Si quieres que se apague la pantalla del esp32 se puede elegir el tiempo en espera hasta que se apague.
Apagado automático de la pantalla desactivado
Tiempo para apagado automático (ms):

=== Configuración sonda(s) de temperatura

/ Control de Temperatura:
	Activamos para hacer uso de las sondas.

/ Temperatura de Encendido:
	Temperatura de encendido del pwm. Referencia: sonda termo
/ Temperatura de Apagado:
	Temperatura de apagado del pwm. Referencia: sonda termo

/ Modo automático: Activación del pwm según temperatura configurada estando en modo automático. Valor pwm según configuración potencia objetivo, producción, consumo casa...

/ Modo Manual: Activación del pwm según temperatura configurada estando en modo manual. Valor pwm fijo según el parámetro  “porcentaje de pwm en modo manual”.

Recordar que el modo Manual puede ir sujeto al programador horario.   

/ Dirección Sensor Termo: Seleccione un sensor
/ Dirección Sensor Triac: Seleccione un sensor
/ Nombre Sensor Personalizado: Dirección 
/ Sensor Personalizado: Seleccione un sensor

==== Control de temperatura mínima

Condiciones de activación:

- "PWM (ON)" en la pantalla principal, si está en OFF no hará nada.
- que no esté en modo manual (ya sea activado por freeDS o por el usuario)
- que no exista error en la lectura de la sonda de temperatura
- que no se encuentre en ese momento derivando potencia al termo (excedentes solares)
- y que por supuesto esté activada la opción de mantener la temperatura mínima.

Desactivación del control de temperatura mínima:

- si no ha entrado en funcionamiento el programador horario o
- si se encuentra en modo manual (activado por freeds)
- si la temperatura mínima se ha superado en 3 grados.


Funcionamiento:

Sí el agua no está suficientemente caliente (por debajo del min programado), Freeds cambia a modo manual y calienta el agua hasta superar en 3 grados la consigna mínima, al finalizar el modo vuelve a automático.

Freeds usa el % de PWN que tenemos programado en modo manual como límite de potencia del termo.

Sí por otros consumos ocasionales en la vivienda se superará la potencia contratada, freeds regula el PWN para no superar dicha potencia. (Importante que el dato de potencia contratada sea real y esté configurado)

Se puede programar este control de temperatura mínimo independientemente del control de temperatura automático o manual, es decir, no es necesario activar dichos controles.

// === Configuración Alexa

// Control mediante alexa desactivado

