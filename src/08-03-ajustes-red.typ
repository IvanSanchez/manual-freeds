== Ajustes de red <ajustes-red>

#figure(
  image("ajustes-red.png"),
  caption: [Ajustes de red]
)

/ SSID Wifi 1:  en este desplegable saldrán las distintas redes wifi y su cobertura a menor número mejor señal. Seleccionamos la que queremos e introducimos la contraseña para que FreeDS se conecte a la red. Siempre son redes de 2,4 Ghz. FreeDS, de momento no funciona con redes 5Ghz
/ SSID Wifi 2 : igual que el anterior, pero nos deja configurar una segunda red wifi por si la primera no estuviera disponible.
/ Nombre de Host: aquí introduciremos el nombre que mostrará FreeDS en la red o herramientas de rastreo de señal como “brokers MQTT”.
/ Dirección IP: es la dirección IP que tendrá nuestro derivador en la red doméstica. Podemos dejar que sea nuestro router quien se la asigne activando “ obtención por DHCP”, tendrás que usar un programa de rastreo (por ejemplo “Fing”), para saber la IP asignada. Se recomienda que la conviertas en fija o estática ,desde tu router o puedes fijarla desde el propio FreeDS, con lo que tienes que asegurarte que la dirección IP elegida está libre, y no entrar en conflicto con otro dispositivo. Para fijarla a través de FreeDS, se desactiva el botón de “obtención por DHCP” y se cubren los campos.

/ Puerta de enlace: es la dirección ip de nuestra salida a internet, habitualmente es la dirección ip de nuestro router principal.
/ Máscara de red: es la combinación que delimita las ips de nuestra red, normalmente como está en la imagen 255.255.255.0
/ DNS 1: es donde pondremos la ip de nuestro servidor dns preferido, normalmente los de google,  8.8.8.8 o cloudfire 1.1.1.1
/ DNS 2: igual que la anterior pero con las dns secundarias, normalmente los dns de google 8.8.4.4 o cloudfire 1.0.0.1
/ Dirección DHCP: Activa o desactiva la opción dhcp para permitir o no que nuestro router principal entregue ip al dispositivo FreeDS o por el contrario introducirla manualmente.

Después de configurar usar el botón "guardar".

=== Cómo poder actuar con el FreeDS dentro y fuera del alcance de la red WiFi

Si nuestro router, por ejemplo un Mikrotik, nos permite establecer una VPN elegir esta opción por razones de seguridad.

Alternativa: Apertura de puertos

Ejemplo práctico:

Inversor HUAWEI conectado a la WiFi "de casa" y con ip fija. Por ejemplo `192.168.1.60`

Freeds conectado a la misma WiFi "de casa" y tb con ip fija. Por ejemplo `192.168.1.20`

Se configura el  FreeDS para Huawei y se introduce la ip del inversor Huawei para que lea los datos del mismo: `192.168.1.60`

Si se desea ver los datos estando en "casa", se abre en el navegador (móvil u ordenador) la ip del freeds `192.168.1.20` se introduce usuario y contraseña (por defecto ambas son “admin”)

Si se desea ver los datos estando fuera de "casa" (fuera del alcance de la  WiFi de casa):

En el caso de tener IP pública fija, me la apunto. Si no la tengo, puedo crear una cuenta tipo ddns.net (es gratis). Por ejemplo, "miipdecasa.ddns.net"

En el router abro un puerto, por ejemplo 60220 a la IP del freeds (192.168 1.20).
Así desde fuera, cuando quiero acceder al freeds, en navegador pondremos: "mi_ip_decasa.ddns.net:60220".

Metemos usuario y contraseña de FreeDS y listo (recordad que por defecto son: admin)

Al mismo tiempo, habría que meter en el router la gestión DNS para que vaya actualización automáticamente

