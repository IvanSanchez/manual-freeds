#import "template-aviso.typ": aviso

= Arquitecturas de placas de potencia <arquitecturas-potencia>

== Conexiones de la placa de control <conexiones-esp32>

Cualquier placa de potencia deberá conectarse a la placa de control según estas conexiones. En este diagrama se incluyen todos los accesorios que se pueden conectar a la placa de control.

Las placas diseñadas específicamente para FreeDS cuentan con un zócalo para encajar la placa de control, y no suele ser necesario preocuparse por el conexionado. Hay que asegurarse, eso sí, de que la placa de control está conectada en la orientación correcta.

#figure(
  image("assets/conexiones-esp32.png"),
  caption: [Esquema de conexiones del ESP32]
)

/ PWM: pin 25. Estrictamente necesario, conectado al TRIAC de la placa de potencia.
/ Relés: pines 13, 12, 14, 27
/ Sonda(s) de temperatura: pin 2
/ Pulsador externo: pin 0
/ RS485 para modbus: RX por pin 19, TX por pin 23
/ ESP-01: RX por pin 17, TX por pin 5. Se usa para conectividad extra, pero suele bastar con la antena WiFi de la placa de control
/ Pinza amperimétrica: pin 34 (No se muestra aquí; véase #link("pinza-amperimetrica")[Pinza amperimétrica])

La placa de potencia debe proveer 5V+tierra a la placa de control. La placa de control provee 3.3V+tierra a sus accesorios.

== Arquitectura PCB FreeDS

Las placas FreeDS son placas diseñadas, por  y para el proyecto, es una variante del ULTRA LOW COST en la versión V2 funcionando generalmente a 10Hz. Hay versiones antiguas V1 que son del tipo low cost funcionando a 500Hz. Para calcular $R_b$, sería: $R_b ≃ 9/"Iv"$ donde Iv es la corriente del ventilador.

La @placa-freeds es una foto de la pcb con todos los componentes soldados. Las PCB llevan grabado en el reverso la versión, o también  Model 2 y la revisión Rev. 3.0. Las versiones son cambios significativos y las revisiones son cambios más superfluos.

#figure(
  image("assets/arquitecturas/pcb-freeds.png", width: 50%),
  caption: [Placa FreeDS]
) <placa-freeds>

#figure(
  image("assets/arquitecturas/pcb-freeds-rev1.png"),
  caption: [Diagrama de placa FreeDS, revisión 1]
)

#figure(
  image("assets/arquitecturas/pcb-freeds-rev2.png"),
  caption: [Diagrama de placa FreeDS, revisión 2]
)

== Arquitectora Letón <arquitectura-leton>

Esta es la arquitectura original que se montaba con el firmware predecesor IQAS. Usa una placa PWM ya montada que se adquiere a través de #link("https://www.tindie.com/products/bugrovs2012/pwm-16a-3500w-triac-leading-edge-dimmer-50hz-60hz/")[Tindie]. Las placas las monta Olegs Bugrovs, de Letonia ("el letón"), bajo la marca "KRIDA electronics".

Este es su esquema de conexión:

#figure(
  image("assets/arquitecturas/arquitectura-leton.png"),
  caption: [Diagrama de placa Leton]
)

Manual completo: #link("https://drive.google.com/file/d/1iHRqanpghea6BxVxn-jnkLNJI8eYi--y/view?usp=sharing")

En la placa, es necesario posicionar el puente “pwm input level” en $3.3V$.


#figure(
  image("assets/arquitecturas/arquitectura-leton-pcb.png", width: 50%),
  caption: [Placa Leton]
)

== Arquitectura Letón prov5 <arquitectura-leton-prov5>

Una actualización de Olegs Bugrovs sobre su arquitectura anterior. El rasgo distintivo de esta arquitectura es que la placa de control ya no es un ESP32 Heltec blanco, sino una placa propia con un ESP32-WROOM-32UE. Esta placa de control permite tener una antena externa (para mejor recepción de Wifi) y conectores para todos los periféricos (relés, sondas de temperatura, RS485 para modbus, etc).

#box[
  #columns(2, [
    #figure(
      image("assets/arquitecturas/leton2/potencia.jpg"),
      caption: [Placa de potencia Letón prov5]
    )
  #colbreak()
    #figure(
      image("assets/arquitecturas/leton2/control.jpg"),
      caption: [Placa de control Letón prov5]
    )
  ])
]

Al igual que en la anterior arquitectura Letón, estas placas se venden a través de #link("https://www.tindie.com/products/bugrovs2012/pwm10a-2200w-mosfet-trailing-edge-dimmer-freeds/")[Tindie].

Estas placas soportan una carga máxima de 10A (que a 220V quiere decir 2200W). Como en la arquitectura anterior, es necesario posicionar el puente “pwm input level” en $3.3V$.

== Arquitectura Low Cost (Configurar a 500Hz) <arquitectura-low-cost>

Es una arquitectura que puedes hacer tú mismo, y la desarrolló Rafael González. Se basa en utilizar un módulo _dimmer_ de bajo coste ya montado.

#box[
  #columns(2, [
    #figure(
      image("assets/arquitecturas/arquitectura-lowcost-pcb-1.png"),
      caption: [Placa Low Cost, trasera]
    )
  #colbreak()
    #figure(
      image("assets/arquitecturas/arquitectura-lowcost-pcb-2.png"),
      caption: [Placa Low Cost, frontal]
    )
  ])
]

Otra arquitectura muy popular, de Low cost, es la ensamblada por SolaresTV siguiendo el esquema de Rafael Gonzalez.

Este es el esquema:

#figure(
  image("assets/arquitecturas/arquitectura-lowcost.png"),
  caption: [Diagrama de placa Low Cost]
)

== Arquitectura Ultra Low Cost o Gen3 (Configurar a 10Hz o 12,5Hz)

#link("https://www.youtube.com/watch?v=fUT3yco4m_Q&t=94s")

Esta arquitectura sigue los desarrollos de #link("https://www.mk2pvrouter.co.uk/index.html")[Robin Emley]

#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen3.png", width: 50%),
  caption: [Diagrama de MOC3041 en Low Cost Gen3]
)

Es muy popular por su sencillez de montaje y fiabilidad. Basado en la tecnología de optoacoplador MOC3041.

Este es el esquema del Gen3:

#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen3-2.png"),
  caption: [Diagrama de placa Low Cost Gen3]
)

Se puede agregar un ventilador al esquema anterior y que sea gobernado automáticamente por el software. La conexión es muy simple:

#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen3-ventilador.png", width: 50%),
  caption: [Diagrama de ventilador en placa Low Cost Gen3]
)

La ubicación de los componentes se podría situar como se ve en la imagen, R4 y el transistor usando también dos pines para facilitar la conexión del ventilador.


#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen3-pcb.png", width: 50%),
  caption: [Placa Low Cost Gen3]
)

Ojo, pin 5V puede variar según marca DiY More o Heltec

El sistema funciona a *10 Hz* de frecuencia de pwm.
Este sistema NO funciona bien con los inversores tipo voltronic debido a que funciona a una frecuencia de 10hz y el medidor del inversor va más rápido. En este caso hay que montar la versión low cost, que funciona a 500hz.

Este es el manual con video tutoriales para montar tu GEN3 paso a paso.
#link("https://drive.google.com/file/d/1D_W0472yuDgfqKMn9xPWBLxG_nkHphXo/view?usp=sharing")

== Arquitectura Ultra Low Cost o Gen4 (Configurar a 10Hz o 12,5Hz)

Podemos adquirir una placa preparada para soldar (ver enlace en lista de componentes), se trata del montaje que corresponde con el diseño Gen4.

Trucos para montar tu PCB: #link("https://youtu.be/CRHT1q5eHrU")

#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen4-1.png", width: 50%),
  caption: [Placa Low Cost Gen4]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen4-2.png", width: 50%),
  caption: [Placa Low Cost Gen4]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen4-3.png", width: 50%),
  caption: [Placa Low Cost Gen4]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen4-4.png", width: 50%),
  caption: [Placa Low Cost Gen4]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen4-5.png", width: 50%),
  caption: [Placa Low Cost Gen4]
)


== Arquitectura Ultra Low Cost/Gen9 Blue Edition (Configurar a 10Hz)

La arquitectura Generación 9 nace como la evolución de la placa oficial (gracias Lolo) para incorporar soluciones como la salida de SSR con modulación gemela que duplica la salida moduladas del gestor permitiendo trabajar con potencias superiores a $3500W$, refuerza la fuente de alimentación, aumenta el grosor de las pistas de potencia, soluciona el calentamiento de los clips del fusible de versiones anteriores, incorpora un triac mucho más potente y mejora la disipación de calor gracias a la ventilación forzada reforzada con dos ventiladores. También incorpora un sensor DS18B20 en placa para control de temperatura del triac.

El Gen9 Blue Edition se distingue por su característica PCB azul. #link("https://youtu.be/NVk6N2sWeFs")

#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen9-pcb.png", width: 60%),
  caption: [Placa Low Cost Gen9]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen9-caja.png"),
  caption: [Caja con Low Cost Gen9]
)
#figure(
  image("assets/arquitecturas/arquitectura-lowcost-gen9.png"),
  caption: [Diagrama Low Cost Gen9]
)

#link("https://github.com/SolaresTV/FreeDS-SolaresTV/blob/main/ASSEMBLY.pdf")

== Arquitectura #link("https://es.wikipedia.org/wiki/Relé_de_estado_sólido")[SSR]. Modo Gestor

#aviso[¡MUY IMPORTANTE! El SSR para ser usado en modo gestor debe tener paso por 0. #underline[Esto es crítico y no debe obviarse jamás]. Si no sabes que es, #underline[NO LO USES]]

La arquitectura más sencilla de ensamblar gracias al uso de un SSR, muy útil para implementar 40A ó 80A de potencia sin complicarse.

Este sistema SSR *NO* funciona bien con los inversores tipo voltronic debido a que funciona a una frecuencia de 10hz y el medidor del inversor va más rápido. En este caso hay que montar la versión low cost, que funciona a 500hz.

Esquema y link a memoria y videotutoriales:
#link("https://drive.google.com/file/d/1Ramk1B7m2sx-duEHeVl9B7gSj4xDYqCU/view?usp=sharing")[MEMORIA PARA GESTOR DE EXCEDENTES CON ARQUITECTURA SSR .pdf]

#figure(
  image("assets/arquitecturas/arquitectura-ssr-gestor.png"),
  caption: [Diagrama SSR modo gestor]
)

== Arquitectura #link("https://es.wikipedia.org/wiki/Relé_de_estado_sólido")[SSR]. Modo relé para control de motor de depuradora.

Esta arquitectura es útil para el control por SSR de depuradoras.

Esquema y link a memoria y videotutoriales:
#link("https://drive.google.com/file/d/1-Pwm3PRK2Ibqbnh2u8zZGeTZIe_Rwt-0/view?usp=sharing ")

#figure(
  image("assets/arquitecturas/arquitectura-ssr-depuradora.png"),
  caption: [Diagrama SSR para depuradora]
)

== Arquitectura SSVR

La arquitectura ultralowcost aprovecha la salida pwm de un esp32 para disparar un triac en un instante concreto, sin ningún microcontrolador o electrónica compleja por el medio. La arquitectura Lowcost o Leton utiliza la misma salida pwm de un esp32 como medio para indicarle a una electrónica secundaria a qué porcentaje debe trabajar. 

Esta nueva arquitectura, utiliza un medio distinto. El esp32 dispone de una salida analógica que va 0÷3,3V 0÷100% pin 26.  Con esta arquitectura da igual que valor tenga el parámetro frecuencia pwm ya que no utilizamos la salida pwm pin 25. La salida analógica del esp32 es de 8bit, con lo cual vamos a tener 256 escalones vs los 12bit del pwm, que son 4096 escalones. 

A continuación se propone un montaje que aprovecha la señal analógica del esp32 y la convertimos en otro tipo de señal muy utilizada en ámbitos industriales de tipo 4…20mA 0÷100%. Con eso hacemos el freeDS mucho más compatible con productos comerciales.  En el siguiente ejemplo se propone un dimmer concreto , pero en la plataforma Aliexpress hay un montón de dimmers disponibles igual de válidos.

#figure(
  image("assets/arquitecturas/arquitectura-ssvr.png"),
  caption: [Diagrama SSVR]
)

Propuestas de material:
- Convertidor 0÷3,3VDC a 4…20mA: https://s.click.aliexpress.com/e/_9xebu3
- Dimmer: https://s.click.aliexpress.com/e/_A5COnH
- Disipador (este es muy grande, puede ser más pequeño o con otro formato): https://s.click.aliexpress.com/e/_Af5wdZ
- Magnetotérmico https://www.leroymerlin.es/fp/16634261/interruptor-magnetotermico-chint-bipolar-10a

== Arquitectura Clinxer <arquitectura-clinxer>

Esta arquitectura fue diseñada por el usuario \@clinxer, con los objetivos de:

- Tener conectores para todos los periféricos
- Tener antena para wifi
- Poder soldar o bien un TRIAC o bien una placa _dimmer_ ya montada
- Caber dentro de las cajas estancas populares en otras arquitecturas

El diseño del PCB de esta arquitectura está en https://github.com/pablozg/freeds/tree/PID/hardware/clinxer ; para visualizar el diseño (y/o para generar los ficheros "gerber" necesarios para fabricar el circuito impreso) se usa el software de diseño de circuitos #link("https://www.kicad.org/")[KiCad]. El diseño incluye también el listado de componentes.

Esta placa no está disponible comercialmente (que se sepa); hace falta pedir la fabricación de los circuitos y soldar los componentes uno mismo.

De manera similar a la arquitectura Letón prov5, la placa de control no se basa en un ESP32 Heltec blanco, sino en un ESP32-WROOM-UE

#box[
  #columns(2, [
    #figure(
      image("assets/arquitecturas/clinxer/potencia.jpg", width: 60%),
      caption: [Placa de potencia clinxer]
    )
  #colbreak()
    #figure(
      image("assets/arquitecturas/clinxer/control.jpg"),
      caption: [Placa de control clinxer]
    )
  ])
]

== Modificación onda sinusoidal 50Hz 230V según arquitectura

#figure(
  image("assets/arquitecturas/onda.png", width: 50%),
  caption: [Ondas según arquitectura]
)

== Instalación del gestor en un termo con gestión electrónica

Con las arquitecturas tradicionales alimentamos el aparato directamente con los pulsos del pwm. Para un termo tradicional compuesto básicamente por un termostato y una resistencia no es ningún problema. Sin embargo, para una electrónica, ser alimentada de esa forma puede suponer un serio problema. Es por ello que la solución es instalar el gestor posterior a la electrónica. 

#aviso[Modificar cualquier aparato eléctrico de manera incorrecta puede ocasionar serios problemas. Debemos de tener en cuenta que si pasamos a seguir las indicaciones a partir de estas líneas, lo hacemos bajo nuestra propia responsabilidad. Si no sabes lo que haces, por favor, deja que la instalación la haga un profesional.]

En realidad cualquier arquitectura de las que hemos visto es compatible con un termo electrónico si tenemos en cuenta dos cosas:
+ El esp32 debe de ser alimentado de forma independiente. 
+ El gestor debe de conectarse entre la electrónica del termo y su resistencia. 

Debemos de tener en cuenta que la instalación interna de los aparatos varían de un modelo a otro. Así que deberemos adaptarla a nuestro modelo.  En grandes rasgos se compone de una placa electrónica, una sonda de temperatura y una resistencia. 

#figure(
  image("instalacion-termo.png"),
  caption: [Instalación en termo]
)
Ojo, pin 5V puede variar según marca DiY More o Heltec

Después de la instalación del gestor es recomendable realizar la siguiente prueba: https://youtu.be/n2m3yZzYW44

