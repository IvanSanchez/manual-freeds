== Reiniciar

Para reiniciar el FreeDs: visitar http://ipfreeds/reboot. En algunos casos es posible que no se reinicie y se debe restaurar a “Fábrica” pulsando 10 segundos el botón PRG en el propio ESP32.

En los casos más extremos será necesario reinstalar FreeDS conectándolo directamente al PC mediante el USB, para ello es necesario desconectarlo de la placa debido a la resistencia de la patilla 2 que puentea para las sondas de temperatura.
