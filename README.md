
# El manual en PDF está en https://ivansanchez.gitlab.io/manual-freeds/manual-freeds.pdf



Este es el manual de [FreeDS](http://freeds.es), maquetado con [`typst`](https://github.com/typst/typst).

Para editar el manual hay que editar los ficheros en `src/`. Se recomienda leer el [tutorial de `typst`](https://typst.app/docs/tutorial/).

